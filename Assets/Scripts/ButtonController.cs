using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    [SerializeField] private string gameScene = "GameScene";
    public void Play()
    {
        SceneManager.LoadScene(gameScene);
    }
}
