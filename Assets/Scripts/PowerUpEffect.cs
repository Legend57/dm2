using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpEffect : MonoBehaviour
{
    public float powerUpSize = 5f;
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Powerup"))
        {
            PlayerController playerController = other.gameObject.GetComponent<PlayerController>();
            playerController.globalControl.playerHealth += powerUpSize;
            gameObject.transform.localScale = Vector3.zero;
            GetComponent<AudioSource>().Play();
        }
    }
}
