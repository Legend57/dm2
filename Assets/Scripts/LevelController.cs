using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class LevelController : MonoBehaviour
{
    public GameObject rightPrefab;
    public GameObject leftPrefab;
    public GameObject backgroundPrefab;
    public GameObject powerUpPrefab;
    public GameObject powerDownPrefab;
    public GlobalController globalControl;
    private List<GameObject> _spawnedWalls = new List<GameObject>();
    private List<GameObject> _powerUps = new List<GameObject>();
    private List<GameObject> _powerDowns = new List<GameObject>();
    private List<GameObject> _backgroundTiles = new List<GameObject>();
    
    private Transform _playerTransform;
    private float safeZone = 80.0f;
    private float powerUpDeleteZone = 20.0f;

    private float _spawnZ;
    private float _tileLength;
    private int tilesOnScreen = 6;

    private void Start()
    {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        _tileLength = rightPrefab.transform.localScale.z;
        
        InvokeRepeating(nameof(HandleSpeed), globalControl.levelIncreaseInterval, globalControl.levelIncreaseInterval);
        
        CreateWall(true);
        CreateWall(false);
        CreateBackgroundTile();
        for (int i = 0; i < tilesOnScreen; i++)
        {
            _spawnZ += _tileLength;
            CreateWall(true);
            CreateWall(false);
            CreateBackgroundTile();
        }
    }

    void Update()
    {
        HandleGravity();
        if (_playerTransform.position.z - safeZone > (_spawnZ - _tileLength * tilesOnScreen))
        {
            _spawnZ += _tileLength;
            HandleWalls();
            HandlePowerUps((int)Math.Max(1, 4 - globalControl.gameStage));
            HandlePowerDowns((int)Math.Max(1,Math.Floor(globalControl.gameStage / 2)));
        }
    }
    
    void HandleWalls()
    {
        CreateWall(true);
        CreateWall(false);
        CreateBackgroundTile();
        transform.position = new Vector3(0, 1, _spawnZ);
        if (_spawnedWalls.Count >= tilesOnScreen)
        {
            DestroyWall(1);
            DestroyWall(0);
            RemoveBackgroundTile();
            RemovePassedPowerUpsAndDowns();
        }
        
    }

    private void RemovePassedPowerUpsAndDowns()
    {
        for (int i = _powerUps.Count - 1; i >= 0; i--)
        {
            if (_powerUps[i].transform.position.z < _playerTransform.position.z - powerUpDeleteZone)
                DestroyPowerUps(i);
        }
        for (int i = _powerDowns.Count - 1; i >= 0; i--)
        {
            if (_powerDowns[i].transform.position.z < _playerTransform.position.z - powerUpDeleteZone)
                DestroyPowerDowns(i);
        }
    }

    void DestroyWall(int index)
    {
        GameObject destroyWall = _spawnedWalls[index];
        _spawnedWalls.RemoveAt(index);
        Destroy(destroyWall);
    }
    void RemoveBackgroundTile()
    {
        GameObject destroyBgTile = _backgroundTiles[0];
        _backgroundTiles.RemoveAt(0);
        Destroy(destroyBgTile);
    }
    
    void DestroyPowerUps(int index)
    {
        GameObject destroyPowerUp = _powerUps[index];
        _powerUps.RemoveAt(index);
        Destroy(destroyPowerUp);
    }
    
    void DestroyPowerDowns(int index)
    {
        GameObject destroyPowerDown = _powerDowns[index];
        _powerDowns.RemoveAt(index);
        Destroy(destroyPowerDown);
    }

    void CreateWall(bool right)
    {
        GameObject wallPrefab;
        if (right)
            wallPrefab = rightPrefab;
        else
            wallPrefab = leftPrefab;
        Vector3 defaultPosition = wallPrefab.transform.position;
        Vector3 spawnPoint = new Vector3(defaultPosition.x, defaultPosition.y, _spawnZ);
        GameObject newWall = Instantiate(wallPrefab, spawnPoint, Quaternion.identity);
        _spawnedWalls.Add(newWall);
    }

    void CreateBackgroundTile()
    {
        Vector3 defaultPosition = backgroundPrefab.transform.position;
        Vector3 spawnPoint = new Vector3(defaultPosition.x, defaultPosition.y, _spawnZ);
        GameObject newBackgroundTile = Instantiate(backgroundPrefab, spawnPoint, Quaternion.identity);
        _backgroundTiles.Add(newBackgroundTile);
    }

    void HandlePowerUps(int spawnRate)
    {
        for (int i = 0; i < spawnRate; i++)
        {
            float randomOffsetZ = Random.Range(_spawnZ + 30, _spawnZ - 30);
            float randomOffsetX = Random.Range(5, -5);
            Vector3 spawnPoint = new Vector3(randomOffsetX, 1, randomOffsetZ);
            GameObject newPowerUp = Instantiate(powerUpPrefab, spawnPoint, Quaternion.identity);
            _powerUps.Add(newPowerUp);
        }
    }
    
    void HandlePowerDowns(int spawnRate)
    {
        for (int i = 0; i < spawnRate; i++)
        {
            float randomOffsetZ = Random.Range(_spawnZ + 30, _spawnZ - 30);
            float randomOffsetX = Random.Range(6, -6);
            if (i % 3 == 0)
            {
                float powerDownOffset = Random.Range(-2, 2);
                float randomXV1 = Random.Range(7, 5);
                randomOffsetX = randomXV1;
                if (powerDownOffset < 0)
                    randomOffsetX *= -1;
            }
            Vector3 spawnPoint = new Vector3(randomOffsetX, 1, randomOffsetZ);
            GameObject newPowerDown = Instantiate(powerDownPrefab, spawnPoint, Quaternion.identity);
            _powerUps.Add(newPowerDown);
        }
    }
    
    void HandleSpeed()
    {
        globalControl.levelSpeed += globalControl.levelIncreaseRate;
        float remainder = globalControl.levelSpeed % 20;
        globalControl.gameStage = (globalControl.levelSpeed - remainder) / 20;
    }
    
    void HandleGravity()
    {
        float coef = globalControl.playerHealth * 0.5f;
        if (coef < 20)
            coef = 20;
        if (Physics.gravity.x < 0) coef *= -1;
        Physics.gravity = new Vector3(coef, Physics.gravity.y, Physics.gravity.z);
    }  
    
}
