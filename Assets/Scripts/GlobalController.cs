using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalController : MonoBehaviour
{
    public float levelSpeed = 15f;
    public float levelIncreaseRate = 1f;
    public float levelIncreaseInterval = 10f;
    public float gameStage = 1;
    public float playerHealth = 100f;
    public float lifeDecreaseRate = 1f;
    private float _lifeDecreaseTime = 1f;
    private bool isRunning = true;
    public bool negativeGravity;
    public GameObject gameOverScreen;
    public Vector3 defaultGravity = new Vector3(-20f, 0f, 20f);
    public AudioSource ambientMusic;

    public Timer levelTimer;

    private void Start()
    {
        gameOverScreen.SetActive(false);
        Physics.gravity = defaultGravity;
        levelTimer.StartTimer();
        ambientMusic.Play();
    }

    private void FixedUpdate()
    {
        if (Time.time > _lifeDecreaseTime && isRunning)
        {
            playerHealth -= gameStage;
            _lifeDecreaseTime = Time.time + lifeDecreaseRate;
        }
    }

    public void KillSpeed()
    {
        isRunning = false;
        playerHealth = 0;
        levelSpeed = 0;
        levelTimer.StopTimer();
        ambientMusic.Stop();
        StartCoroutine(DelayedKill(1));
    }
    IEnumerator DelayedKill(float time)
    {
        yield return new WaitForSeconds(time);
        gameOverScreen.SetActive(true);
    }

}
