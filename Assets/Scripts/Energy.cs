using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Energy : MonoBehaviour
{

    public GlobalController controller;
    public Slider slider;

    private void Update()
    {
        slider.value = controller.playerHealth;
    }
}
