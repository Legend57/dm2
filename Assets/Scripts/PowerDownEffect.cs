using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerDownEffect : MonoBehaviour
{
    public float powerDownSize = 20f;
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Powerup"))
        {
            PlayerController playerController = other.gameObject.GetComponent<PlayerController>();
            playerController.globalControl.playerHealth -= powerDownSize;
            gameObject.transform.localScale = Vector3.zero;
            GetComponent<AudioSource>().Play();
        }
    }
}
