using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;
    public Vector3 cameraOffset = new Vector3(0, 20, -2);
    public GlobalController globalControl;
    void FixedUpdate()
    {
        if (globalControl.playerHealth > 0)
        {
            Vector3 playerPosition = player.position;
            cameraOffset.x = -playerPosition.x;
            Vector3 desiredPosition = playerPosition + cameraOffset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, globalControl.levelSpeed * Time.deltaTime);
            transform.position = smoothedPosition;
        }
    }
}
