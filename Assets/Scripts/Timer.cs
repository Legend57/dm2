using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private float _currentTime;
    public bool timerActive;
    private bool _removeTutorial;
    public TextMeshProUGUI currentTimeText;
    public TextMeshProUGUI tutorialPrefab;
    private TextMeshProUGUI _tutorial;
    private void Start()
    {
        _currentTime = 0;
    }
    private void Update()
    {
        if (timerActive)
        {
            _currentTime = _currentTime + Time.deltaTime;
        }
        TimeSpan time = TimeSpan.FromSeconds(_currentTime);
        currentTimeText.text = time.ToString(@"mm\:ss\:fff");
        if (_removeTutorial)
        {
            
        }
        if (time.Seconds > 9 && _removeTutorial)
        {
            Destroy(_tutorial);
            _removeTutorial = false;
        }
    }

    public void StartTimer()
    {
        timerActive = true;
        _tutorial = Instantiate(tutorialPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        _tutorial.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
        _removeTutorial = true;

    }
    public void StopTimer()
    {
        timerActive = false;
    }
}
