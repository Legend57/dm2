using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Compression;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody _playerBody;
    private MeshRenderer _playerMeshRenderer;
    public GlobalController globalControl;
    public Color rightColor = new Color(1f, 0.268f, 0f);
    public Color rightEmission = new Color(2.996f, 0.329f, 0f);
    public Color leftColor = new Color(0f, 1f, 0.843f);
    public Color leftEmission = new Color(0.290f, 1.498f, 1.373f);
    private float slowDown = 3f;
    private bool audioIsPlaying;

    private void Start()
    {
        _playerBody = GetComponent<Rigidbody>();
        _playerMeshRenderer = GetComponent<MeshRenderer>();
        transform.position = new Vector3(0, 1, 0);
    }
    private void Update()
    {
        if (Input.GetKeyDown("space"))
            ReverseGravity();
        if (transform.position.x >= 7 || transform.position.x <= -7)
        {
            if (!audioIsPlaying)
            {
                GetComponent<AudioSource>().Play();
                audioIsPlaying = true;
            }
        }
        else if (audioIsPlaying)
        {
            GetComponent<AudioSource>().Stop();
            audioIsPlaying = false;
        }
    }
    void FixedUpdate()
    {
        float currentSpeed = globalControl.levelSpeed;
        Color currentEmmission = _playerMeshRenderer.material.GetColor("_EmissionColor");
        float emmissionIntensity = globalControl.playerHealth * 100 / 200;
        emmissionIntensity = emmissionIntensity * 10 / 100;
        currentEmmission.a = emmissionIntensity;
        _playerMeshRenderer.material.SetColor("_EmissionColor", currentEmmission);
        if (globalControl.playerHealth <= 0)
        {
            globalControl.KillSpeed();
            _playerBody.freezeRotation = true;
            _playerMeshRenderer.material.SetColor("_Color", new Color(0,0,0,1));
            _playerMeshRenderer.material.SetColor("_EmissionColor", new Color(0,0,0,0));
            if (slowDown > 0)
            {
                currentSpeed = slowDown - Time.deltaTime;
                slowDown = currentSpeed;
            }
            else
                _playerBody.constraints = RigidbodyConstraints.FreezeAll;
        }
        _playerBody.velocity = Vector3.ClampMagnitude(_playerBody.velocity, currentSpeed);
        Vector3 desiredVelocity = new Vector3(_playerBody.velocity.x, _playerBody.velocity.y, globalControl.levelSpeed);
        Vector3 smoothedVelocity = Vector3.Lerp(_playerBody.velocity, desiredVelocity, currentSpeed * Time.deltaTime);
        _playerBody.velocity = smoothedVelocity;
        if (currentSpeed <= 0)
            _playerBody.velocity = Vector3.zero;
    }
    private void ReverseGravity()
    {
        Color currentEmmission = _playerMeshRenderer.material.GetColor("_EmissionColor");
        Color newEmmission;
        if (globalControl.negativeGravity)
        {
            newEmmission = rightEmission;
            _playerMeshRenderer.material.SetColor("_Color", rightColor);
            globalControl.negativeGravity = false;
        }
        else
        {
            newEmmission = leftEmission;
            _playerMeshRenderer.material.SetColor("_Color", leftColor);
            globalControl.negativeGravity = true;
        }
        newEmmission.a = currentEmmission.a;
        _playerMeshRenderer.material.SetColor("_EmissionColor", newEmmission);
        Vector3 reverseGravity = new Vector3(-Physics.gravity.x, Physics.gravity.y, Physics.gravity.z);
        Physics.gravity = reverseGravity;
    }
}
